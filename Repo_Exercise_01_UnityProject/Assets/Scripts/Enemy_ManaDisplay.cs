﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Enemy_ManaDisplay : Enemy
{
    public bool grounded;
    public bool floating;
    public bool IsAttackStrong;
    public bool IsAttackMagical;
    public string SpecialAttack;
    public string AttackName;
    public string Damage;
    public float ManaDisplay;
    public float ManaCount;
    public int ManaCost;
    public int ManaChange;
}
