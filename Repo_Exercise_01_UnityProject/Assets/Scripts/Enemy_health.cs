﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Enemy_health : Enemy
{
    public bool grounded;
    public bool floating;
    public bool IsDemonAlive;
    public bool IsDemonLayingOnTheGround;
    public string DemonName;
    public string AttackName;
    public string Damage;
    public float HealthBar;
    public float HealthDisplay;
    public int MaxHealth;
    public int MinHealth;
}
