﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Demon : Enemy
{
    public bool grounded;
    public bool floating;
    public bool IsDemonAlive;
    public bool IsDemonLayingOnTheGround;
    public string DemonName;
    public string AttackName;
    public string Damage;
    public float HealthDisplay;
    public float ManaDisplay;
    public int LevelDisplay;
    public int EXPPoints;
}
