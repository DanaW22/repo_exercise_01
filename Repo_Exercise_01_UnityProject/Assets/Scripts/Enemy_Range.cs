﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Enemy_Range : Enemy
{
    public bool grounded;
    public bool floating;
    public bool IsPlayerInFront;
    public bool IsPlayerDead;
    public string DemonSpecialAttack;
    public string AttackName;
    public string Damage;
    public float AttackSkill;
    public float Attack;
    public int AttackRange;
    public int AttackPoints;
}
