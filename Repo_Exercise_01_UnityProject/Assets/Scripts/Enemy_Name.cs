﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Enemy_Name : Enemy
{
    public bool grounded;
    public bool floating;
    public bool OverTheHead;
    public bool InFrontOfTheBody;
    public float EXPPoints;
    public float Mana;
    public int Health;
    public int InformationDisplay;
    public string Name;
    public string NickName;
    public string SpecialAttack;
    
}
