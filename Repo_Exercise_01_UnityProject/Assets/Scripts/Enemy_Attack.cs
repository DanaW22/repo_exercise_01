﻿using UnityEngine;
using System.Collections;
using CoAHomework;

public class Enemy_Attack : Enemy
{
    public bool grounded;
    public bool floating;
    public bool IsPlayerAlive;
    public bool IsPlayerOnTheGround;
    public string SpecialAttack;
    public string AttackName;
    public string Damage;
    public float ManaPoints;
    public float AttackPoints;
    public int AttackStrength;
    public int HealthPoints;
}
